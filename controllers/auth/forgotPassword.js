import { ERROR_MESSAGE } from "../../utils/constants";
import UserModel from "../../models/UserModel";
import bcrypt from "bcryptjs";

const forgotPassword = async (req, res) => {
  const { email } = req.body;
  if (!email) {
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const newPassword = generatePassword();
    const newPasswordHashed = await bcrypt.hash(newPassword, 10);
    // UserModel.findOneAndUpdate(
    //   { email: email },
    //   { password: newPasswordHashed },
    //   { new: true },
    //   (err, user) => {
    //     if (err) res.status(500).json(ERROR_MESSAGE);
    //     else if (!user) {
    //       res.status(400).json(ERROR_MESSAGE);
    //     } else {
    //       res.status(200).json({
    //         message: "New password sent to your email address",
    //       });
    //     }
    //   }
    // );
    res.status(400).json({ message: "Success" });
  }
};

export default forgotPassword;

function generatePassword() {
  let chars = "QWERTYUIOPASDFGHJKLZCVBNMabcdefghijklmnopqrstuvwxyz0123456789";
  let password = "";
  for (let i = 0; i < 10; i++) {
    password += chars[Math.floor(Math.random() * chars.length)];
  }
  return password;
}
