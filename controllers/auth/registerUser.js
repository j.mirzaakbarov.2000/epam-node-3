import UserModel from "../../models/UserModel";
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from "../../utils/constants";
import bcrypt from "bcryptjs";
import chalk from "chalk";

const registerUser = (req, res) => {
  const { role, password, email } = req.body;
  if (!role || !password || !email) {
    console.log(chalk.red("Insufficient credentials!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    UserModel.findOne({ email }, async (err, user) => {
      if (err) {
        res.status(500).json(ERROR_MESSAGE);
      } else {
        if (user == null) {
          UserModel.create(
            {
              email,
              password: await bcrypt.hash(password, 10),
              role,
              created_date: new Date(),
            },
            (err, user) => {
              if (err) {
                res.status(500).json(ERROR_MESSAGE);
              } else {
                console.log(
                  chalk.green(
                    `Registered a new user : role - ${user.role} , email - ${user.email}`
                  )
                );
                res.status(200).json({
                  message: "Profile created successfully",
                });
              }
            }
          );
        } else {
          console.log(
            chalk.yellow(`User with email ${user.email} already exists !`)
          );
          res.status(400).json(ERROR_MESSAGE);
        }
      }
    });
  }
};

export default registerUser;
