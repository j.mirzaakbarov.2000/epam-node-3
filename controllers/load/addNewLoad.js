import { ERROR_MESSAGE } from "../../utils/constants";
import jwt from "jsonwebtoken";
import LoadModel from "../../models/LoadModel";
import chalk from "chalk";

const addNewLoad = (req, res) => {
  const [, token] = req.headers["authorization"].split(" ");
  const fields = [
    "name",
    "payload",
    "pickup_address",
    "delivery_address",
    "dimensions",
  ];
  const dimensions = ["width", "length", "height"];
  let hasAllFields = true;
  fields.forEach((field) => {
    if (!req.body[field]) {
      hasAllFields = false;
    } else if (field === "dimensions" && req.body[field]) {
      dimensions.forEach((dimension) => {
        if (!req.body[field][dimension]) {
          hasAllFields = false;
        }
      });
    }
  });
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else if (!hasAllFields) {
    console.log(chalk.red("Insufficient body params!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    if (myData.role !== "SHIPPER") {
      console.log(chalk.red("Unauthorized !"));
      res.status(400).json(ERROR_MESSAGE);
    } else {
      LoadModel.create(
        {
          created_by: myData._id,
          assigned_to: null,
          status: "NEW",
          state: "Uploaded",
          name: req.body.name,
          payload: req.body.payload,
          pickup_address: req.body["pickup_address"],
          delivery_address: req.body["delivery_address"],
          dimensions: req.body.dimensions,
          logs: [
            {
              message: "Load created",
              time: new Date(),
            },
          ],
          created_date: new Date(),
        },
        (err, load) => {
          if (err) {
            res.status(500).json(ERROR_MESSAGE);
          } else {
            console.log(
              chalk.green(
                `Created a new load '${load.name}' , id '${load._id}'`
              )
            );
            res.status(200).json({
              message: "Load created successfully",
            });
          }
        }
      );
    }
  }
};

export default addNewLoad;
