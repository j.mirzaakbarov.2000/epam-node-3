import { ERROR_MESSAGE } from "../../utils/constants";
import jwt from "jsonwebtoken";
import LoadModel from "../../models/LoadModel";
import TruckModel from "../../models/TruckModel";
import UserModel from "../../models/UserModel";
import chalk from "chalk";

const changeLoadState = (req, res) => {
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    if (myData.role !== "DRIVER") {
      console.log(chalk.red("Unauthorized !"));
      res.status(400).json(ERROR_MESSAGE);
    } else {
      LoadModel.findOne(
        {
          assigned_to: myData._id,
          status: "ASSIGNED",
        },
        (err, load) => {
          if (err) {
            res.status(500).json(ERROR_MESSAGE);
          } else if (!load) {
            console.log(chalk.red(`No active load for DRIVER ${myData.email}`));
            res.status(400).json(ERROR_MESSAGE);
          } else {
            if (load.state === "Arrived to delivery") {
              console.log(
                chalk.red(`Load ${load._id} is already in the final state!`)
              );
              res.status(400).json(ERROR_MESSAGE);
            } else {
              const nextState = (function (load) {
                const states = [
                  "En route to Pick Up",
                  "Arrived to Pick Up",
                  "En route to delivery",
                  "Arrived to delivery",
                ];
                for (let i = 0; i < states.length; i++) {
                  if (states[i] === load.state) {
                    return states[i + 1];
                  }
                }
              })(load);
              if (nextState === "Arrived to delivery") {
                load.status = "SHIPPED";
                UserModel.findOne({ _id: load.assigned_to }, (err, user) => {
                  if (err) console.log(err);
                  else {
                    TruckModel.findOneAndUpdate(
                      { created_by: user._id },
                      { assigned_to: null, status: "IS" },
                      (err, truck) => {
                        console.log(
                          chalk.green(
                            `Delivery '${load._id}' was shipped successfully, disassigning truck '${truck._id}'`
                          )
                        );
                      }
                    );
                  }
                });
              }
              load.state = nextState;
              load.logs = [
                ...load.logs,
                {
                  message: `Load state changed to '${nextState}'`,
                  time: new Date(),
                },
              ];
              load.save((err) => {
                if (err) res.status(500).json(ERROR_MESSAGE);
                else {
                  console.log(
                    chalk.yellow(
                      `Load state for load '${load._id}' changed to '${nextState}'`
                    )
                  );
                  res.status(200).json({
                    message: `Load state changed to '${nextState}'`,
                  });
                }
              });
            }
          }
        }
      );
    }
  }
};

export default changeLoadState;
