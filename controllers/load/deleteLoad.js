import { ERROR_MESSAGE } from "../../utils/constants";
import LoadModel from "../../models/LoadModel";
import chalk from "chalk";
import jwt from "jsonwebtoken";

const deleteLoad = (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    if (myData.role !== "SHIPPER") {
      console.log(chalk.red("Unauthorized !"));
      res.status(400).json(ERROR_MESSAGE);
    } else {
      LoadModel.findOneAndDelete(
        { _id: id, created_by: myData._id, status: "NEW" },
        (err, load) => {
          if (err) {
            res.status(500).json(ERROR_MESSAGE);
          } else {
            if (!load) {
              console.log(
                chalk.red(`No load with id '${id}' found or Unauthorized`)
              );
              res.status(400).json(ERROR_MESSAGE);
            } else {
              console.log(chalk.green(`Deleted load with id '${id}'`));
              res.status(200).json({
                message: "Load deleted successfully",
              });
            }
          }
        }
      );
    }
  }
};

export default deleteLoad;
