import { ERROR_MESSAGE } from "../../utils/constants";
import LoadModel from "../../models/LoadModel";
import chalk from "chalk";
import jwt from "jsonwebtoken";

const getActiveLoad = async (req, res) => {
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    if (myData.role !== "DRIVER") {
      console.log(chalk.red("Unauthorized !"));
      res.status(400).json(ERROR_MESSAGE);
    } else {
      LoadModel.findOne(
        {
          assigned_to: myData._id,
          status: "ASSIGNED",
        },
        (err, load) => {
          if (err) {
            res.status(500).json(ERROR_MESSAGE);
          } else {
            if (!load) {
              console.log(
                chalk.red(`No active load found for DRIVER ${myData.email}`)
              );
              res.status(400).json(ERROR_MESSAGE);
            } else {
              const {
                _id,
                created_by,
                assigned_to,
                status,
                state,
                name,
                payload,
                pickup_address,
                delivery_address,
                dimensions,
                logs,
                created_date,
              } = load;
              res.status(200).json({
                load: {
                  _id,
                  created_by,
                  assigned_to,
                  status,
                  state,
                  name,
                  payload,
                  pickup_address,
                  delivery_address,
                  dimensions,
                  logs,
                  created_date,
                },
              });
            }
          }
        }
      );
    }
  }
};

export default getActiveLoad;
