import { ERROR_MESSAGE } from "../../utils/constants";
import LoadModel from "../../models/LoadModel";
import TruckModel from "../../models/TruckModel";
import UserModel from "../../models/UserModel";
import chalk from "chalk";
import jwt from "jsonwebtoken";

const TRUCK_TYPES = {
  SPRINTER: { width: 300, length: 250, height: 170, payload: 1700 },
  "SMALL STRAIGHT": { width: 500, length: 250, height: 170, payload: 2500 },
  "LARGE STRAIGHT": { width: 700, length: 350, height: 200, payload: 4000 },
};
const dimensions = ["width", "length", "height"];

const postLoad = (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("Insufficient body params or No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    if (myData.role !== "SHIPPER") {
      console.log(chalk.red("Unauthorized!"));
      res.status(400).json(ERROR_MESSAGE);
    } else {
      LoadModel.findOne(
        {
          _id: id,
          created_by: myData._id,
        },
        (err, load) => {
          if (err) {
            res.status(500).json(ERROR_MESSAGE);
          } else {
            if (!load) {
              console.log(
                chalk.red(`No load with id '${id}' found or Unauthorized!`)
              );
              res.status(400).json(ERROR_MESSAGE);
            } else if (load.status !== "NEW") {
              console.log(chalk.red("Load has already been posted"));
              res.status(400).json(ERROR_MESSAGE);
            } else {
              load.status = "POSTED";
              load.logs.push({
                message: "Load posted",
                time: new Date(),
              });
              load.save((err, load) => {
                if (err) res.status(500).json(ERROR_MESSAGE);
                else {
                  TruckModel.find({ status: "IS" }, (err, trucks) => {
                    if (err) res.status(500).json(ERROR_MESSAGE);
                    else {
                      let truck = null;
                      for (let i = 0; i < trucks.length; i++) {
                        let isAppropriateTruck = true;
                        dimensions.forEach((dimension) => {
                          if (
                            TRUCK_TYPES[trucks[i].type][dimension] <
                            load.dimensions[dimension]
                          ) {
                            isAppropriateTruck = false;
                          }
                        });
                        if (isAppropriateTruck && trucks[i].assigned_to) {
                          truck = trucks[i];
                          break;
                        }
                      }
                      if (truck) {
                        LoadModel.findOne({ _id: id }, (err, load) => {
                          if (err) res.status(500).json(ERROR_MESSAGE);
                          else {
                            load.assigned_to = truck.created_by;
                            load.status = "ASSIGNED";
                            load.state = "En route to Pick Up";
                            load.logs.push({
                              message: `Load assigned to driver with id ${truck.created_by}`,
                              time: new Date(),
                            });
                            load.save((err) => {
                              if (err) res.status(500).json(ERROR_MESSAGE);
                              else {
                                TruckModel.findOneAndUpdate(
                                  { _id: truck._id },
                                  {
                                    status: "OL",
                                  }
                                );
                                console.log(
                                  chalk.green(`Posted load with id '${id}'`)
                                );
                                res.status(200).json({
                                  message: "Load posted successfully",
                                  driver_found: true,
                                });
                              }
                            });
                          }
                        });
                      } else {
                        console.log(
                          chalk.red("Couldn't find approriate truck")
                        );
                        LoadModel.findOne({ _id: id }, (err, load) => {
                          if (err) res.status(500).json(ERROR_MESSAGE);
                          else {
                            load.status = "NEW";
                            load.logs.push({
                              message: `Changing status to 'NEW'`,
                              time: new Date(),
                            });
                            load.save((err) => {
                              if (err) res.status(500).json(ERROR_MESSAGE);
                              else {
                                res.status(400).json(ERROR_MESSAGE);
                              }
                            });
                          }
                        });
                      }
                    }
                  });
                }
              });
            }
          }
        }
      );
    }
  }
};

export default postLoad;
