import { ERROR_MESSAGE } from "../../utils/constants";
import TruckModel from "../../models/TruckModel";
import chalk from "chalk";
import jwt from "jsonwebtoken";
import UserModel from "../../models/UserModel";

const removeTruck = (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    TruckModel.findOneAndDelete(
      { _id: id, created_by: myData._id, status: "IS", assigned_to: null },
      (err, truck) => {
        if (err) {
          res.status(500).json(ERROR_MESSAGE);
        } else {
          if (!truck) {
            console.log(
              chalk.red(
                `No truck with id '${id}' found or truck is ON LOAD or Unauthorized`
              )
            );
            res.status(400).json(ERROR_MESSAGE);
          } else {
            if (truck.assigned_to) {
              UserModel.findByIdAndUpdate(truck.assigned_to, {
                assigned_to: null,
              });
            }
            console.log(chalk.green(`Deleted truck with id '${id}'`));
            res.status(200).json({
              message: "Truck deleted successfully",
            });
          }
        }
      }
    );
  }
};

export default removeTruck;
