import { ERROR_MESSAGE } from "../../utils/constants";
import TruckModel from "../../models/TruckModel";
import chalk from "chalk";
import jwt from "jsonwebtoken";

const updateTruck = (req, res) => {
  const { type } = req.body;
  const { id } = req.params;
  const [, token] = req.headers["authorization"].split(" ");
  if (!type || !token) {
    console.log(chalk.red("Insufficient body params or No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else if (
    type !== "LARGE STRAIGHT" &&
    type !== "SMALL STRAIGHT" &&
    type !== "SPRINTER"
  ) {
    console.log(chalk.red("Invalid truck type!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    TruckModel.findOneAndUpdate(
      {
        _id: id,
        created_by: myData._id,
        assigned_to: null,
        status: "IS",
      },
      { type: type },
      (err, truck) => {
        if (err) {
          res.status(500).json(ERROR_MESSAGE);
        } else {
          if (!truck) {
            console.log(
              chalk.red(
                `No truck with id '${id}' found or truck is ON LOAD , or Unauthorized!`
              )
            );
            res.status(400).json(ERROR_MESSAGE);
          } else {
            console.log(
              chalk.green(
                `Updating the type of the truck with id '${id}' to '${type}' for DRIVER ${myData.email}`
              )
            );
            res.status(200).json({
              message: "Truck details changed successfully",
            });
          }
        }
      }
    );
  }
};

export default updateTruck;
