import { ERROR_MESSAGE } from "../../utils/constants";
import jwt from "jsonwebtoken";
import UserModel from "../../models/UserModel";
import chalk from "chalk";
import TruckModel from "../../models/TruckModel";

const deleteMyInfo = (req, res) => {
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    TruckModel.findOne(
      { status: "OL", created_by: myData._id },
      (err, truck) => {
        if (err) res.status(500).json(ERROR_MESSAGE);
        else if (truck) {
          console.log(chalk.red("Can't delete account while ON LOAD!"));
          res.status(400).json(ERROR_MESSAGE);
        } else {
          UserModel.deleteOne({ email: myData.email }, (err) => {
            if (err) res.status(500).json(ERROR_MESSAGE);
            else {
              console.log(chalk.green(`Deleted user ${myData.email}`));
              res.status(200).json({
                message: "Profile deleted successfully",
              });
            }
          });
        }
      }
    );
  }
};

export default deleteMyInfo;
