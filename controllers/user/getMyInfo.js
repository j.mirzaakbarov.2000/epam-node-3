import { ERROR_MESSAGE } from "../../utils/constants";
import jwt from "jsonwebtoken";
import chalk from "chalk";
import UserModel from "../../models/UserModel";

const getMyInfo = (req, res) => {
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    UserModel.findById(myData._id, (err, user) => {
      if (err) res.status(500).json(ERROR_MESSAGE);
      else {
        if (!user) {
          console.log(
            chalk.red(`Invalid token or no user ${myData.email} found!`)
          );
          res.status(400).json(ERROR_MESSAGE);
        } else {
          console.log(
            chalk.green(`Sending personal info for user ${myData.email}`)
          );
          res.status(200).json({
            user: {
              _id: myData._id,
              role: myData.role,
              email: myData.email,
              created_date: myData.created_date,
            },
          });
        }
      }
    });
  }
};

export default getMyInfo;
