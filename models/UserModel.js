import mongoose from "mongoose";

const UserSchema = mongoose.Schema({
  role: String,
  email: String,
  password: String,
  created_date: Date,
});

export default mongoose.model("UserModel", UserSchema);
